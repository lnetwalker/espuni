<!-- SPDX-FileCopyrightText: 2021 Benedikt Spranger <b.spranger@linutronix.de> -->
<!-- SPDX-License-Identifier: CERN-OHL-S-2.0 -->

Main goal of ESP-Uni is to have a base platform for ESP01 based projects. 

![ESP01-uni schematics](https://gitlab.com/lnetwalker/espuni/raw/master/images/ESPuni-schematics.png)

# IO Pin description

| Signal Name | Header/Pin | alternate Name|
|-------------|------------|---------------|
| gpio 0      |  J1/3      |               |
| gpio 1      |  J104/2    | TxD           |
| gpio 2      |  J1/2      |               |
| gpio 3      |  J104/1    | RxD           |

ESP-Uni is based on the work of the KLIMA project by Benedikt Spranger. 

For the original work look at the KLIMA project . https://github.com/eurovibes/klima/

![klima logo](https://github.com/eurovibes/klima/raw/master/images/klima.png "klima logo")


## Copyright and License

Copyright Benedikt Spranger 2021.
modified by Hartmut Eilers 2022.
This source describes Open Hardware and is licensed under the CERNOHL-S v2.
You may redistribute and modify this source and make products using it
under the terms of the CERN-OHL-S v2
(https://ohwr.org/cern ohl s v2.txt ).
This source is distributed WITHOUT ANY EXPRESS OR IMPLIED
WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY
QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
the CERN-OHL-S v2 for applicable conditions.
Source location: https://github.com/eurovibes/klima
As per CERN-OHL-S v2 section 4, should You produce hardware based
on this source, You must where practicable maintain the Source Location
visible on the external case of the klima or other products you make using
this source.
